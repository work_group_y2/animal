/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.animal;

/**
 *
 * @author ทักช์ติโชค
 */
public class Fish extends AquaticAnimal {

    private String species;

    public Fish(String species) {
        super("Fish");
        this.species = species;
    }

    @Override
    public void swim() {
        System.out.println("Fish: " + species + " swim"); 
    }

    @Override
    public void eat() {
        System.out.println("Fish: " + species + " eat"); 
    }

    @Override
    public void walk() {
       System.out.println("Fish: " + species + " walk"); 
    }

    @Override
    public void speak() {
        System.out.println("Fish: " + species + " speak"); 
    }

    @Override
    public void sleep() {
        System.out.println("Fish: " + species + " sleep"); 
    }

}
