/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.animal;

/**
 *
 * @author ทักช์ติโชค
 */
public abstract class Reptile extends Animal {

    public Reptile(String name, int number0fLeg) {
        super(name, number0fLeg);
    }

    public abstract void crawl();
}
