/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.animal;

/**
 *
 * @author ทักช์ติโชค
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.sleep();
        h1.run();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is landanimal ? " + (h1 instanceof LandAnimal));

        Animal a1 = h1;
        System.out.println("a1 is animal ? " + (a1 instanceof Animal));
        System.out.println("a1 is reptile animal ? " + (a1 instanceof Reptile));

        Puppy p1 = new Puppy("Parrot");
        p1.eat();
        p1.fly();
        p1.speak();

        System.out.println("p1 is animal ? " + (p1 instanceof Animal));
        System.out.println("p1 is poultry animal ? " + (p1 instanceof Poultry));

        Animal a2 = p1;
        System.out.println("a2 is animal ? " + (a2 instanceof Animal));
        System.out.println("a2 is reptile animal ? " + (a2 instanceof Reptile));

        Fish f1 = new Fish("Dolphin");
        f1.eat();
        f1.swim();
        f1.sleep();

        System.out.println("f1 is animal ? " + (f1 instanceof Animal));

        Animal a3 = f1;
        System.out.println("a3 is animal ? " + (a3 instanceof Animal));
        System.out.println("a3 is landanimal ? " + (a3 instanceof LandAnimal));
        System.out.println("a3 is reptile animal ? " + (a3 instanceof Reptile));

        Snake s1 = new Snake("Yellow");
        s1.crawl();
        s1.speak();
        s1.sleep();

        System.out.println("s1 is animal ? " + (s1 instanceof Animal));

        Animal a4 = s1;
        System.out.println("a3 is animal ? " + (a4 instanceof Animal));
        System.out.println("a3 is landanimal ? " + (a4 instanceof LandAnimal));
        System.out.println("a3 is reptile animal ? " + (a4 instanceof AquaticAnimal));

        Cat c1 = new Cat("Sphynx");
        c1.eat();
        c1.run();
        c1.sleep();

        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is landanimal ? " + (c1 instanceof LandAnimal));

        Animal a5 = c1;
        System.out.println("a5 is animal ? " + (a5 instanceof Animal));
        System.out.println("a5 is reptile animal ? " + (a5 instanceof Reptile));

        Crab cr1 = new Crab("Field");
        cr1.eat();
        cr1.swim();
        cr1.speak();

        System.out.println("cr1 is animal ? " + (cr1 instanceof Animal));

        Animal a6 = cr1;
        System.out.println("a6 is animal ? " + (a6 instanceof Animal));
        System.out.println("a6 is landanimal ? " + (a6 instanceof LandAnimal));
        System.out.println("a6 is reptile animal ? " + (a6 instanceof Reptile));

        Bat b1 = new Bat("Batman");
        b1.fly();
        b1.eat();
        b1.speak();

        System.out.println("b1 is animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is poultry animal ? " + (b1 instanceof Poultry));

        Animal a7 = b1;
        System.out.println("a2 is animal ? " + (a7 instanceof Animal));
        System.out.println("a2 is reptile animal ? " + (a7 instanceof Reptile));
        
        Crocodile cc1 = new Crocodile("Alligator");
        cc1.crawl();
        cc1.sleep();
        cc1.speak();

        System.out.println("cc1 is animal ? " + (cc1 instanceof Animal));

        Animal a8 = cc1;
        System.out.println("a8 is animal ? " + (a8 instanceof Animal));
        System.out.println("a8 is landanimal ? " + (a8 instanceof LandAnimal));
        System.out.println("a8 is reptile animal ? " + (a8 instanceof AquaticAnimal));
        
        Dog d1 = new Dog("Rottweiler");
        d1.eat();
        d1.run();
        d1.speak();

        System.out.println("d1 is animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is landanimal ? " + (d1 instanceof LandAnimal));

        Animal a9 = d1;
        System.out.println("a9 is animal ? " + (a9 instanceof Animal));
        System.out.println("a9 is reptile animal ? " + (a9 instanceof Reptile));
        
        Worm w1 = new Worm("Red");
        w1.eat();
        w1.sleep();
        w1.speak();
     
        System.out.println("w1 is landanimal ? " + (d1 instanceof LandAnimal));

        Animal a10 = w1;
        System.out.println("a10 is animal ? " + (a10 instanceof LandAnimal));
        System.out.println("a10 is reptile animal ? " + (a10 instanceof Reptile));
        System.out.println("a10 is reptile animal ? " + (a10 instanceof Puppy));
        System.out.println("a10 is reptile animal ? " + (a10 instanceof AquaticAnimal));

    }

}
