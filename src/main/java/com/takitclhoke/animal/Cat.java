/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.animal;

/**
 *
 * @author ทักช์ติโชค
 */
public class Cat extends LandAnimal {
    private String species;

    public Cat(String species) {
        super("Cat", 4);
        this.species = species;
    }

    @Override
    public void run() {
       System.out.println("Cat: " + species + " run"); 
    }

    @Override
    public void eat() {
       System.out.println("Cat: " + species + " eat"); 
    }

    @Override
    public void walk() {
        System.out.println("Cat: " + species + " walk"); 
    }

    @Override
    public void speak() {
        System.out.println("Cat: " + species + " speak"); 
    }

    @Override
    public void sleep() {
        System.out.println("Cat: " + species + " sleep"); 
    }
    
}
