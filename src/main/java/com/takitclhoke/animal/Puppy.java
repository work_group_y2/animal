/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.animal;

/**
 *
 * @author ทักช์ติโชค
 */
public class Puppy extends Poultry{
    private String species;

    public Puppy(String species) {
        super("Puppy", 2);
        this.species = species;
    }

    @Override
    public void fly() {
        System.out.println("Puppy: " + species + " fly"); 
    }

    @Override
    public void eat() {
        System.out.println("Puppy: " + species + " eat"); 
    }

    @Override
    public void walk() {
        System.out.println("Puppy: " + species + " walk"); 
    }

    @Override
    public void speak() {
        System.out.println("Puppy: " + species + " speak"); 
    }

    @Override
    public void sleep() {
        System.out.println("Puppy: " + species + " sleep"); 
    }
    
}
