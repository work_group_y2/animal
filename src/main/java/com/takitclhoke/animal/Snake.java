/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.animal;

/**
 *
 * @author ทักช์ติโชค
 */
public class Snake extends Reptile {

    private String species;

    public Snake(String species) {
        super("Sanke", 4);
        this.species = species;
    }

    @Override
    public void crawl() {
        System.out.println("Sanke: " + species + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Sanke: " + species + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Sanke: " + species + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Sankey: " + species + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Sanke: " + species + " sleep");
    }

}
