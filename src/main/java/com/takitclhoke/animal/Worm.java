/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.animal;

/**
 *
 * @author ทักช์ติโชค
 */
public class Worm extends Animal {

    private String species;

    public Worm(String species) {
        super("Worm",0);
        this.species = species;
        
    }

    @Override
    public void eat() {
       System.out.println("Worm: " + species + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Worm: " + species + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Worm: " + species + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Worm: " + species + " sleep");
    }

}
