/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.animal;

/**
 *
 * @author ทักช์ติโชค
 */
public abstract class Animal {

    private String name;
    private int number0fLeg;

    public Animal(String name, int number0fLeg) {
        this.name = name;
        this.number0fLeg = number0fLeg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber0fLeg() {
        return number0fLeg;
    }

    public void setNumber0fLeg(int number0fLeg) {
        this.number0fLeg = number0fLeg;
    }

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", number0fLeg=" + number0fLeg + '}';
    }
    
    public abstract void eat();
    public abstract void walk();
    public abstract void speak();
    public abstract void sleep();
    

}
