/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.animal;

/**
 *
 * @author ทักช์ติโชค
 */
public class Crocodile extends Reptile {
    
    private String species;

    public Crocodile(String species) {
        super("Crocodile", 4);
        this.species = species;
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile: " + species + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile: " + species + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Crocodile: " + species + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile: " + species + " speak");
    }

    @Override
    public void sleep() {
       System.out.println("Crocodile: " + species + " sleep");
    }
    
}
