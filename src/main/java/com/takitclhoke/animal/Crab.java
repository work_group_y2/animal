/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.animal;

/**
 *
 * @author ทักช์ติโชค
 */
public class Crab extends AquaticAnimal {

    private String species;

    public Crab(String species) {
        super("Crab");
        this.species = species;
    }

    @Override
    public void swim() {
        System.out.println("Crab: " + species + " swim");
    }

    @Override
    public void eat() {
        System.out.println("Crab: " + species + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Crab: " + species + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Crab: " + species + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crab: " + species + " sleep");
    }

}
